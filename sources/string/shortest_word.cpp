/* Shortest Word
Simple, given a string of words, return the length of the shortest word(s).
String will never be empty and you do not need to account for different data types.


It(Sample_Test_Cases)
  {
    Assert::That(find_short("bitcoin take over the world maybe who knows perhaps"), Equals(3));
    Assert::That(find_short("turns out random test cases are easier than writing out basic ones"), Equals(3));
    Assert::That(find_short("lets talk about javascript the best language"), Equals(3));
    Assert::That(find_short("i want to travel the world writing code one day"), Equals(1));
    Assert::That(find_short("Lets all go on holiday somewhere very cold"), Equals(2));
    Assert::That(find_short("Let's travel abroad shall we"), Equals(2));
  }
*/
#include <iostream>
#include <string>
using namespace std;

int main(){
    string text="bitcoin take over the world maybe who knows perhaps";
    string word="";
    int minLength=100;
    for (auto x:text){
          if (x==' '){
              if(word.length()<minLength) minLength=word.length();
              word="";
          }else{
              word+=x;
          }
      }
    if(word.length()<minLength) minLength=word.length();
cout<<minLength<<endl;
   return 0;
}

