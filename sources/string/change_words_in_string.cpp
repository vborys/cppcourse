/* Simple Pig Latin
Move the first letter of each word to the end of it, then add "ay" to the end of the word. Leave punctuation marks untouched.

Examples
pigIt('Pig latin is cool'); // igPay atinlay siay oolcay
pigIt('Hello world !');     // elloHay orldway !
*/

//=== "Solution 1 (*my)"
#include <bits/stdc++.h>
using namespace std;

string pig_it(string str)
{
    string result="";
    string word = "";
    for (auto x : str){
       if (x == ' ')
        {
           if (word=="!" || word=="?" || word=="." || word=="," ) {
               result.append(word+" ");
               word = "";
           }
           else{

            word.append(string(1,word.at(0))+"ay");
            word.erase(0,1);
            result.append(word+" ");
            word = "";
          }

        }
        else {
            word = word + x;
        }
    }
    if (word=="!" || word=="?" || word=="." || word=="," ){
    result.append(word);
    } else {
    word.append(string(1,word.at(0))+"ay");
    word.erase(0,1);
    result.append(word);
    }
return result;
}

int main()
{
    string str = ". q Hello world !";
    cout<<pig_it(str)<<endl;
    return 0;
}


//=== "Solution 2"

#include <bits/stdc++.h>

std::string pig_it(std::string str )
{
  str = str + ' ';
  std::string word;
  std::string newWord;

  for(size_t i {0}; i <= str.size();i++){
    if(std::ispunct(str[i])){
      newWord = newWord + str[i] + ' ';
    }
    else if(str[i] != ' '){
      word += str[i];
      }
    else {
      if (word.size() == 0)
         continue;
      word = word + word.front() + "ay";
      word.erase(0,1);
      newWord += word;
      newWord += ' ';
      word = "";
    }
  }
  newWord.pop_back();

    return newWord;
}

int main()
{
    std::string str = ". q Hello world !";
    std::cout<<pig_it(str)<<std::endl;
    return 0;
}
