/* If you can't sleep, just count sheep!!
Given a non-negative integer, 3 for example, return a string with a murmur: 

"1 sheep...2 sheep...3 sheep...". 

Input will always be valid, i.e. no negative integers.
*/

#include <iostream>
#include <string>
using namespace std;

string countSheep(int number) {
    string result="";
    if(number==0)
        return "";
    else{
    for(int i=1; i<=number; i++)
      result+=(to_string(i)+" sheep...");
    return result;
    }
}

int main()
{
    int n;
    cout<<"How many ships you're count?"<<endl;
    cin>>n;
    cout<<countSheep(n);
    return 0;
}

