/* Fake bin
Given a string of digits, you should replace any digit below 5 with '0' and any digit 5 and above with '1'. Return the resulting string.

Note: input will never be an empty string

Assert::That(fakeBin("45385593107843568"), Equals("01011110001100111"));
Assert::That(fakeBin("509321967506747"), Equals("101000111101101"));
Assert::That(fakeBin("366058562030849490134388085"), Equals("011011110000101010000011011"));
Assert::That(fakeBin("15889923"), Equals("01111100"));
Assert::That(fakeBin("800857237867"), Equals("100111001111"));
*/

#include <string>
std::string fakeBin(std::string str){
std::string result="";
    for(char dig: str) {
        if(dig >= '5')
            result+='1';
        else
            result+='0';
        }
    return result;
}


