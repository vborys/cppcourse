/* Multiplication table for number
  
Your goal is to return multiplication table for number that is always an integer from 1 to 10.

For example, a multiplication table (string) for number == 5 looks like below:

1 * 5 = 5
2 * 5 = 10
3 * 5 = 15
4 * 5 = 20
5 * 5 = 25
6 * 5 = 30
7 * 5 = 35
8 * 5 = 40
9 * 5 = 45
10 * 5 = 50

P. S. You can use \n in string to jump to the next line.

Note: newlines should be added between rows, but there should be no trailing newline at the end. If you're unsure about the format, look at the sample tests.
*/
 
#include <iostream>
#include <string>

std::string multi_table(int number)
{
    std::string result = "";
    std::string line = "";
    for (int var = 1; var <= 10; ++var) {
        line = std::to_string(var)+" * "+std::to_string(number)+" = "+std::to_string(var*number)+(var!=10?"\n":"");
        result.append(line);


    }

    return result;
}

int main()
{
int n;
std::cin>>n;
std::cout<<multi_table(n)<<std::endl;

    return 0;
}

