/* Sum of Numbers
Given two integers a and b, which can be positive or negative, find the sum of all the integers between and including them and return it. If the two numbers are equal return a or b.

Note: a and b are not ordered!

Examples (a, b) --> output (explanation)
(1, 0) --> 1 (1 + 0 = 1)
(1, 2) --> 3 (1 + 2 = 3)
(0, 1) --> 1 (0 + 1 = 1)
(1, 1) --> 1 (1 since both are same)
(-1, 0) --> -1 (-1 + 0 = -1)
(-1, 2) --> 2 (-1 + 0 + 1 + 2 = 2)
*/
#include <iostream>
using namespace std;

int get_sum(int a, int b)
{
    int result = 0;
    if(a>b) swap(a,b);
    for (int var = a; var <= b; ++var) {
        result+=var;
    }

    return result;
}


int main()
{
int number1,number2;
cin>>number1>>number2;

cout<< get_sum(number1,number2) <<endl;
    return 0;
}

