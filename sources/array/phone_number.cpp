/* Create Phone Number
Write a function that accepts an array of 10 integers (between 0 and 9), that returns a string of those numbers in the form of a phone number.

Example
createPhoneNumber(int[10]{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}) // => returns "(123) 456-7890"
The returned format must be correct in order to complete this challenge.
Don't forget the space after the closing parentheses!
*/


#include <iostream>
#include <string>
using namespace std;

string createPhoneNumber(const int arr [10]){
  string result="";
  for (int i = 0; i < 10; ++i) {
      switch (i) {
          case 0:
              result+="("+to_string(arr[i]);
              break;
          case 2:
              result+=to_string(arr[i])+") ";
              break;
          case 5:
          result+=to_string(arr[i])+"-";
          break;

      default: result+=to_string(arr[i]);
      }

  }

  return result;
}

int main()
{
    int arr[]={1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
    cout<<createPhoneNumber(arr)<<endl;

    return 0;
}



/* Sample2

#include <string>
using namespace std;
std::string createPhoneNumber(const int arr [10]){
  string number = "";
  for (int i = 0; i < 10; i++)
    number += to_string(arr[i]);

  number.insert(0, "(");
  number.insert(4, ") ");
  number.insert(9, "-");

  return number;
}

*/

/* Sample3

#include <string>

std::string createPhoneNumber(const int arr [10])
{
  std::string str = "(###) ###-####";

  int j = 0;

  for (int i = 0; i < str.length(); i++)
  {
    if (str.at(i) == '#')
      str.replace(i, 1, std::to_string(arr[j++]));
  }

  return str;
}

*/


