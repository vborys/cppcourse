#include <iostream>
using namespace std;

void setRandArr(int arr[], int size);
void getRandArr(int arr[], int size);

int main()
{
    cout<<"Basic operations with dynamic array\n";
    cout<<"Enter size of array:";
    int size=0;
    cin>>size;
    int *intArr = new int[size];

    setRandArr(intArr,size);
    getRandArr(intArr,size);

    delete[] intArr;
    intArr = nullptr;
    
    return 0;
}

void setRandArr(int arr[], int size){
   for (int i = 0; i < size; i++){
    *(arr + i) = rand()%100;
   }
}

void getRandArr(int arr[], int size){
    for (int i = 0; i < size; i++){
    cout<<*(arr + i)<<" ";
    }
}


