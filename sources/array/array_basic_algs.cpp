/*
Basic operations and algorithms with array in functions
*/

#include <iostream>

using namespace std;

void setArray(int arr[], int size);

void getArray(int arr[], int size);

int sumArray(int arr[], int size);

float averageArray(int arr[], int size);

int main()
{
    const int SIZE=5;
    int arr[SIZE]={};
    setArray(arr, SIZE);
    getArray(arr, SIZE);
    cout<<" Sum:"<<sumArray(arr, SIZE)<<endl;
    cout<<" Average:"<<averageArray(arr, SIZE)<<endl;

    return 0;
}


void setArray(int arr[], int size){
    cout<<"Enter "<<size<<" value:";
    for (int i = 0; i < size; ++i) {
        cin>>arr[i];
    }
}

void getArray(int arr[], int size){
    cout<<"Input array:";
    for (int i = 0; i < size; ++i) {
        cout<<arr[i]<<" ";
    }
}

int sumArray(int arr[], int size){
    int sum=0;
    for (int i = 0; i < size; ++i) {
        sum+=arr[i];
    }
    return sum;
}

float averageArray(int arr[], int size){
    float avrg= sumArray(arr, size)/size;
    return avrg;
}

