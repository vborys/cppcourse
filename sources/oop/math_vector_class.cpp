#include <iostream>
#include <cmath> 
using namespace std;

class Vector {
private:
  int x;
  int y;
  int z;
public:
// Конструктор за замовчуванням
Vector() {
  x = 0;
  y = 0;
  z = 0;
}

// Конструктор
Vector(int tempX, int tempY, int tempZ) {
  x = tempX;
  y = tempY;
  z = tempZ;
}

// Конструктор копіювання
Vector(const Vector& other) {
  x = other.x;
  y = other.y;
  z = other.z;
}


// Функції для доступу до закритих полів класу
  int getX() const {
    return x;
  }
  int getY() const {
    return y;
  }
  int getZ() const {
    return z;
  }

// Метод для знаходження довжини вектора
  double length() {
    return sqrt(x*x + y*y + z*z);
  }

// Метод який виводить координати вектора на екран
  void Out() {
    std::cout << "(" << x << ", " << y << ", " << z << ")" << std::endl;
  }

//Деструктор
~Vector(){}

};

int main(){
    
Vector v(3, 4, 5);
v.Out();
cout<<v.length()<<endl;

return 0;
}
