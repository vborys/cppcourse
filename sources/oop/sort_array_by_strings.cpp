/* Sort array by string length

Write a function that takes an array of strings as an argument and returns a sorted array containing the same strings, ordered from shortest to longest.
For example, if this array were passed as an argument:

["Telescopes", "Glasses", "Eyes", "Monocles"]

Your function would return the following array:

["Eyes", "Glasses", "Monocles", "Telescopes"]

All of the strings in the array passed to your function will be different lengths, so you will not have to decide how to order multiple strings of the same length.
*/

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class Kata{
private:
vector<string> arrayOfStr;

public:
Kata(){
  arrayOfStr={};
}

Kata(vector<string> array){
  arrayOfStr=array;
}
    void sortByLength()
    {
    sort(arrayOfStr.begin(),arrayOfStr.end(), [](string &x, string &y) { return x.length() < y.length(); });
    }

    void setArray(vector<string> array){
    arrayOfStr=array;
    }

    void getArray(){
    for (auto const &element: arrayOfStr)
        cout << element << " ";
    }

~Kata(){}
};

int main(){

Kata kata;
kata.setArray({ "Beg", "Life", "I", "To" });
kata.sortByLength();
kata.getArray();

return 0;
}

