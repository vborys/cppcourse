/* Even or odd sum of array(vector)

*/

#include <iostream>
#include <vector>
#include <string>
#include <numeric>

std::string odd_or_even(const std::vector<int> &arr)
{
    int sum = std::accumulate(arr.begin(), arr.end(), 0);

    return (sum%2!=0) ?  "odd": "even";
}
int main(){
    std::vector<int> mas = {};
    std::cout<<odd_or_even(mas)<<std::endl;
return 0;
}

