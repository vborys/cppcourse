/* Unique In Order (with templates)
Implement the function unique_in_order which takes as argument a sequence and returns a list of items without any elements with the same value next to each other and preserving the original order of elements.

For example:
uniqueInOrder("AAAABBBCCDAABBB") == {'A', 'B', 'C', 'D', 'A', 'B'}
uniqueInOrder("ABBCcAD")         == {'A', 'B', 'C', 'c', 'A', 'D'}
uniqueInOrder([1,2,2,3,3])       == {1,2,3}
*/


#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

template <typename T>
vector<T> uniqueInOrder(vector<T> v) {
  return vector<T>(v.begin(), unique(v.begin(), v.end()));
}

vector<char> uniqueInOrder(string v){
  return vector<char>(v.begin(), unique(v.begin(), v.end()));
}

int main()
{
    vector<char> VC;
    vector<int> VI;

    string str = "AAAABBBCCDAABBB";
    vector<int> arr = {1,2,2,3,3};

    VC = uniqueInOrder(str);
    VI = uniqueInOrder(arr);


    for (auto const element : VC) {
        cout<<element<<' ';
    }
    cout<<endl;
    for (auto const element : VI) {
        cout<<element<<' ';
    }
return 0;
}