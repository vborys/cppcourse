/*Isograms
An isogram is a word that has no repeating letters, consecutive or non-consecutive. Implement a function that determines whether a string that contains only letters is an isogram. Assume the empty string is an isogram. Ignore letter case.

Example: (Input --> Output)
"Dermatoglyphics" --> true
"aba" --> false
"moOse" --> false (ignore letter case)
*/

#include <set>

bool is_isogram(std::string str) {
std::set<char> uniq_set;
for(char letter: str){
    letter = char(tolower(letter));
    uniq_set.insert(letter);
    }
if(uniq_set.size()==str.length())
    return true;
else
    return false;
}

