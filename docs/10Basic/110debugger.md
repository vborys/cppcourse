## Відлагоджувач GDB

### Компіляція та запуск відлагоджувача
```bash
$ g++ -std=c++17 main.cpp -g -o program
$ gdb ./program --tui
```

### Робота у середовищі 
```cpp
(gdb) start
(gdb) step          //вхід у функцію
(gdb) backtrace     //показ стеку
(gdb) info args     //показ використаних змінних
(gdb) print a       //значення змінної
(gdb) print &a      //адреса змінної
(gdb) q             //вихід

```

