!!! info "Визначення:"
    Функція - це іменований набір коду, який може приймати інформацію та повертати значення.


### Види функцій

* стандартні
* користувацькі


## Функції користувача 

```cpp
#include <iostream> 

using namespace std; 

int sum (int,int); //сигнатури (прототипи, описи) функцій
int sum (float,float);

int main(){
    cout<<sum(5,7)<<endl; //виклик функції
    return 0;
} 

int sum (int a,int b){  //опис функції 
    return a+b; 
} 

int sum (float a,float b){ //перегрузка функцій (використання одного імені)
    return a+b; 
} 
```


### Масиви в функціях 
```cpp
void printArray(int arr[], int size) {      //або для захисту даних (const int arr[], int size)
    for(int x=0; x<size; x++) { 
        cout<<arr[x]<<endl; 
    } 
} 

int main() { 
    int myArr[3]= {42, 33, 88}; 
    printArray(myArr, 3); 
}
```


### Рекурсія (виклик самої себе)
```cpp
int factorial(int n){ 
    if (n==1){ 
        return 1;
    } else { 
        return n * factorial(n-1);} 
    } 
int main(){ 
    cout << factorial(5); 
}
```


### Шаблони функції(автовизначення типу) 
```cpp
#include <iostream> 
using namespace std; 
template <typename T> 
T sum (T a, T b) { 
    return a+b; 
    } 
int main() { 
    cout<<sum(5,7)<<endl; 
    return 0;
}
```


### Використання адресації в функціях 
```cpp
#include <iostream>
using namespace std;

void add_two(int &);

int main()
{
    int a = 0;
    cout << "Enter the value: " << endl;
    cin >> a;

    add_two(a);

    cout << "Value out of function a = " << a << endl;
    return 0;
}

void add_two(int &a)
{
    a = a + 2;
    cout << "Value in function a = " << a << endl;
}
```

### Заголовкові файли і модульність програми з функціями

![Image title](../img/functions1.png)


### Додаткові методи роботи з функціями

* [Повернення декількох значень ](https://stackoverflow.com/questions/321068/returning-multiple-values-from-a-c-function)
