!!! info "Визначення:"
    Масив - це іменований набір однотипних даних.

### Види 
* фіксовані
* динамічні

## Фіксовані масиви

### Оголошення масиву

```cpp
int mas[5] = {11, 45, 62, 70, 88}; 
int matrix[2][3]={};
```

### Звернення до елементів масиву

```cpp
cout<< mas[0]<<endl;    //значення
```


### Введення та виведення масиву

```cpp
for(int i=0;i<n;i++) 
  cin>>mas[i];  //cout<<mas[i]; 
```


### Прості алгоритми роботи з масивами 

```cpp
//сума елементів 
int sum = 0; 
for(int i=0;i<n;i++) 
    sum=sum+mas[i];
```

```cpp
//пошук максимального і його номеру
max=a[0];
nmax=0; 
for(i=1;i<n;i++) 
    if (a[i]>max){
        max=a[i]; nmax=i;
    } 
```

```cpp
//сортування вибіркою
int temp; 
for(int i=0;i<n-1;i++){ 
    for(int j=i+1;j<n;j++) 
        if (array[i]>array[j]) {
            temp=array[i]; 
            array[i]=array[j]; 
            array[j]=temp; 
        } 
}
```

```cpp
//сортування методом "бульбашки"
for(int i=0;i<n-1;i++){
        for(int j=n-1;j>i;j--)
            if (mas[j]<mas[j-1]) {
                swap(mas[j],mas[j-1]); //функція заміни елементів
            }
}
```
