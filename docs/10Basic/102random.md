## Випадкові числа 
```cpp
#include <iostream>
//#include <cstdlib>
//#include <ctime>
using namespace std;
int main()
{
    cout<<"Random number:"<<rand()<<endl;       //генерація випадкового числа int
    cout<<"Random number:"<<rand()%100<<endl;   //генерація випадкового числа до 100
    cout<<RAND_MAX<<endl;                       //int max


    srand(3);          //ініціалізація генератора випадкових
    cout<<"Random number:"<<rand()%100<<endl;



    srand(time(NULL));  //ініціалізація генератора функцією часу
    cout<<"Random number:"<<rand()%100<<endl;

    cout<<"Random int number:"<<(rand()%(100-50)+50)<<endl; //діапазон 50-100

    int beforePoint=rand()%10;          //генерація випадкового числа float
    double afterPoint=(rand()%100)/100.0;
    cout<<"Random float number:"<<beforePoint+afterPoint<<endl; //0.00-10.00

    char letter=rand()%('Z'-'A')+'A';   //генерація випадкової літери A-Z
    cout<<"Random letter:"<<letter<<endl;

    return 0;
}


```