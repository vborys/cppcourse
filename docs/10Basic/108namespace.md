### Звернення до простору імен std
```cpp
	std::cout << "Hello world" << std::endl;
```

### Опис власного простору імен з функціями
```cpp
#include <iostream>

namespace NamespaceA {
	void mul(int a, int b) {
		std::cout << (a * b) << std::endl;
	};
}

using namespace NamespaceA;

int main() {	
	mul(10, 42);
	return 0;
}
```

!!! success "Демонстраційне завдання:"
    Створіть перерахування з наступними енумераторами: chicken, lion, giraffe, elephant, duck і snake. Помістіть перерахування в простір імен. Оголосіть масив, де елементами будуть ці енумератори і, використовуючи список ініціалізаторів, ініціалізуйте кожен елемент відповідною кількістю лап певної тварини. У функції main() виведіть кількість ніг у слона, використовуючи енумератор.

```cpp
#include <iostream>
 
namespace Animals
{
    enum Animals
    {
        CHICKEN,
        LION,
        GIRAFFE,
        ELEPHANT,
        DUCK,
        SNAKE,
        MAX_ANIMALS
    };
}
 
int main()
{
    int legs[Animals::MAX_ANIMALS] = { 2, 4, 4, 4, 2, 0 };
 
    std::cout << "An elephant has " << legs[Animals::ELEPHANT] << " legs.\n";
 
    return 0;
}

```