``` cpp
#define PI 3.14 //директива макропідстановки
#define MAXSIZE 100
#define STR "ERROR"
#define ABS(a) (a) < 0 ? -(a) : (a)	// функціональний макрос

#error Програма не допрацьована //директива, яка не дозволяє виконання програми

#include <...>
#include "..."

#undef MAXSIZE //відміна макросу define
```
