### Рядки (як масив char) 

```cpp
#include <сstring> 
char s1[]="Hello world!";   //оголошення 
char s2[20]; 
cin.getline(s2,20);         //введення рядка з пробілами
cin.get(s2,20);             //введення рядка з пробілами без символа кінця рядка

strlen(s1);                 //довжина рядка 
strcat(s1,s2);              //з'єднання рядків 
bool res=strcmp(s1,s2);     //порівняння рядків
strcpy(s1,s2);              //копіює рядок s2 у s1

```

### Рядки (як клас string)
```cpp
#include <string> 
string s1="Hello world!";   //оголошення 
string s2; 
getline(cin,s2);            //введення рядка 

//Перегружені оператори
s1+=s2;                                     //об'єднання рядків 
if (s1==s2) cout<<"Однаковий текст"<<endl;  //порівняння рядків 
else cout<<"Не однаковий"<<endl; 

//Методи роботи з класом string
s1.length();                    //довжина рядка 
s1.at(2);                       //звернення за номером 
auto result = s1.compare(s2);   //порівняння рядків (0-однакові)
s1.substr(2,3);                 //видобування підрядка 
s1.find(s2);                    //пошук позиції входження (string::npos, якщо немає)
s1.replace(pos,len,str);        //заміна частини рядка
s1.insert(5,s2);                //вставка 
s1.swap(s2);                    //обмін рядків

//Конвертація
int num=12;
cout<<to_string(num)<<endl;     //в string

int num=12;
string str="15";
cout<<num+stoi(str)<<endl;      //в int, інші типи: stol(), stof(), stod()

```