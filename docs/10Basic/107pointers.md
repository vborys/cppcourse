### Типи пам'яті
* стек (змінні в головній ф-ції, функціях користувача)
* статична пам'ять (глобальні змінні)
* куча (динамічні змінні через new/delete)

### Опис, створення, види та операції з вказівниками
```cpp
int age=18; 
int *pAge=&age;    //Опис вказівника і визначення адреси змінної
cout<<"Var:"<<*pAge<<" Address: 0x"<<pAge<<endl;  //Операція розіменування вказівника

cin>>*pAge;    //Зчитування даних з використанням вказівника

int *pVoid=nullptr; //Ініціаліація нульового вказівника (C++14)

const int a=5;
const int *pa=&a; 	//вказівник на константу
cout<<&p;			//адреса вказівника

int *const p=&a;	//константний вказівник

const SIZE=5;
int arr[size]={};
cout<<&arr[0];		cout<<arr; //спосіб виклику адреси на масив
int *parr=arr;		//вказівник на масив

int a=1;
int b=2;
int c=3;
int *arr[3]={&a, &b, &c};		//опис масиву вказівників

void (*pfunc1)() = func1;		//спосіб опису вказівників на функції
int (*pfunc2)(int, int) = func2;
```


### Робота з динамічною змінною
```cpp
int *pAge = new int;    //виділення пам'яті для змінної
cout << "Enter your age: ";
cin >> *pAge;
cout << "Age " << *pAge << " is stored at 0x" << pAge << endl;
delete pAge;            //вивільнення пам'яті з видаленням значення змінної

pAge = nullptr;            //занулення вказівника

```


### Динамічні масиви. Арифметика вказівників
```cpp
#include <iostream>

using namespace std;

int main()
{
    int count = 0;
    cout << "Enter size of array: ";
    cin >> count;

    int *array = new int[count];

    for (int i = 0; i < count; i++){
        cout << "Enter number: ";
        cin >> *(array + i);	//арифметика вказівників
    }

    cout << "Display all numbers in array: " << endl;
    for (int i = 0; i < count; i++){
        cout << *(array + i) << " ";
    }

    cout << endl;

    delete[] array;

return 0;
}

//int lengthOfArray = sizeof(array) / sizeof(array[0]); (спосіб визначення довжини динамічного масиву)

```


### Використання вказівників в функціях
```cpp
void ChangeValueWithPointers(int *a, int *b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

int main()
{
	ChangeValueWithPointers(&a, &b);
	cout << "After call ChangeValueWithPointers: a = " << a << ", b = " << b << endl;

	return 0;
}
```

### Передача вказівника масиву в функцію
```cpp
#include <iostream>
using namespace std;

int GetSum(int *arr, int count);	//те саме, що (int arr[] , int count);

int main()
{
	int size = 50;
	int *arr = new int[size];
	for (int i = 0; i < 50; i++)
	{
		*(arr + i) = rand();
	}

	int sum = GetSum(arr, size);

	cout << "Total sum of the array equals = " << sum << endl;

	return 0;
}

int GetSum(int *arr, int count) //реалізація функції обчислення суми елементів
{
	int total = 0;
	for (int i = 0; i < count; i++)	{
		total += *(arr + i);
	}
	return total;
}
```

### Реалізація алгоритмів роботи з масивами з допомогою вказівників
```cpp
#include <iostream>
using namespace std;

void get_array_info(int *arr, int count, int *max, int *min, int *avrg);

int main()
{
	int * p_arr = new int[100];
	for (int i = 0; i < 100; i++){
		*(p_arr + i) = rand();
	}
	int max = 0, min = 0, avrg = 0;
	get_array_info(p_arr, 100, &max, &min, &avrg);	//виклик функції
	cout << "Max element in array : " << max << ", min element = " << min << ", average value = " << avrg << endl;

	return 0;
}

void get_array_info(int *arr, int count, int *max, int *min, int *avrg)
{
	*max = arr[0];
	*min = arr[0];
	*avrg = 0;
	for (int i = 0; i < count; i++)
	{
		if (arr[i] > *max) {
			*max = arr[i];
		}
		if (arr[i] < *min) {
			*min = arr[i];
		}
		*avrg += *(arr + i);
	}
	*avrg = *avrg / count;
}

```


### Вказівники на функції (!!!)






