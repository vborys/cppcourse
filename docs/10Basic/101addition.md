### Область видимості змінної

* створення штучної області видимості:
```cpp
    {
        int b=5;
        cout<<b;
    }
cout<<b; //помилка, вихід за область видимості
```

### Глобальні змінні 

* видимі не тільки у програмі але й у проєкті
* автоматично ініціалізуються 0

```cpp
extern int globalVariable;  //звернення до глобальної змінної з іншого файлу
static int globalVariable;  //обмеження області видимості змінної (тільки у файлі)
```


### Перетворення типів
* неявне 
```cpp
int a=3.4;  //a->3

```

* явне
```cpp
result = (double)a;     //C тип

result = double(a);     //C++ тип

result = static_cast<double>(a);    //C++11 тип
```
