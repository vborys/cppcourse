---
hide:
  - navigation
  - toc
---


![Course Banner](img/bannercpp.png)


# Про цей курс


!!! warning "Зверніть увагу!"
    В курсі представлене авторське бачення теми. У відео можливі випадкові неточності та помилки у зв'язку з онлайн-записом занять. Пропозиції та виправлення приймаються у коментарях. Матеріали у стадії доповнення...


### :hourglass_flowing_sand: Трохи історії

C++ — мова програмування загального призначення створена Б'ярном Страуструпом у 1979 році на основі мови С. Спочатку називалась "Сі з класами", та згодом у 1984 році перейменована на С++. Має підтримку кількох парадигм програмування: об'єктно-орієнтованої, узагальненої, процедурної та ін.

**Приклади коду**

=== "C"

    ``` c
    #include <stdio.h>
    int main(void) {
      printf("Hello world!\n");
      return 0;
    }
    ```

=== "C++"

    ``` c++
    #include <iostream>
    int main(void) {
      std::cout << "Hello world!" << std::endl;
      return 0;
    }
    ```



### :bookmark: Опис розділів  


!!! tip "РОЗДІЛ. Відеокурс"
    [Записи занять](https://vborys.gitlab.io/cppcourse/Course/lessons/) :fontawesome-brands-youtube:{ .youtube } з курсу основ програмування на мові С++.
    
!!! abstract "РОЗДІЛ. Записник::Стартовий"
    Містить опорний конспект з кодом структури програми, основних операції, типів даних. Розглядаються теми: розгалуження, команда множинного вибору, цикли та основи роботи з файлами. 

!!! abstract "РОЗДІЛ. Записник::Базовий"
    Наведені приклади частин коду вищого рівня складності по використанню циклів. Вивчення теми функції, та їх перевантаження. Розглядаються теми: масиви, рядки, вказівники, простори імен а також базові команди роботи зі зневаджувачем GDB.

!!! abstract "РОЗДІЛ. Записник::Просунутий"
    Приклади коду професійного програмування на мові С++. Описано деякі контейнери бібліотеки STL. Користувацькі типи даних. Розпочато розгляд теми ООП.


!!! example "Практика::Код"
    Розміщений вихідний код авторських розв'язків завдань з ресурсу [Codewars](https://www.codewars.com/). Може використовуватись для поглиблення знань та перегляду методів сучасного С++ програмування.


### :star: Післямова 

Курс зроблено [by vborys](https://vborys.gitlab.io/) :man_teacher: для вивчення та підтримки навчання з програмування на мові С++ для себе, моїх учнів, студентів та усіх зацікавлених. Розповсюджується безкоштовно :octicons-heart-fill-24:{ .heart }.




### :books: Джерела:

- [x] [Онлайн-система олімпіадного програмування EOlymp](https://www.eolymp.com/uk/)
- [x] [Відкриті уроки з програмування https://acode.com.ua/](https://acode.com.ua/uroki-po-cpp/)
- [x] [A computer science portal for geeks https://www.geeksforgeeks.org](https://www.geeksforgeeks.org/c-plus-plus/?ref=shm)
- [x] [Programiz: Learn to Code for Free](https://www.programiz.com/dsa)
- [x] [Україномовний канал з програмування @BloganProgramming](https://www.youtube.com/@BloganProgramming)
- [x] [Mike Shah - professor at Northeastern University in the Khoury College of Computer Sciences https://mshah.io/](https://mshah.io/)
- [x] [W3Schools Online Web Tutorials https://www.w3schools.com/cpp/](https://www.w3schools.com/cpp/)
- [x] [Tutorialspoint: Online Tutorials, Courses, and eBooks Library https://www.tutorialspoint.com/cplusplus](https://www.tutorialspoint.com/cplusplus/index.htm)
