### Вказівка вибору

```cpp
if (a==b && b==c) { 
    cout<<"Equals"<<endl; 
    }
    else { 
    cout<<"Not equals"<<endl; 
    }
```

### Множинний вибір 
```cpp
switch (age) { 
    case 16: 
        cout<<"Too young"; 
        break; 
    case 42: 
        cout<<"Adult"; 
        break; 
    case 70: 
        cout<<"Senior"; 
        break; 
    default: cout<<"Error"; 
}
```


### Виведення в форматі true/false 
```cpp 
#include <iomanip> 
... 
bool answer=true;
cout<<boolalpha<<answer;

```