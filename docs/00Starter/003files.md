## Робота з файлами (введення) 

```cpp
#include <fstream> 
ifstream inFile("input.txt"); 
inFile>>n; 
inFile.close(); 
```

### Зчитування з перевіркою

```cpp
#include <fstream>
...
string line;
ifstream inFile("input.txt");
if(inFile.is_open()){
    while(getline(inFile, line)){
        cout<<line<<endl;
        }
    inFile.close();
    }else{
    cout<<"Unable to open file \n";
}
```

## Робота з файлами (виведення) 

```cpp
#include <fstream>
ofstream outFile("output.txt"); 
outFile<<n; 
outFile.close();
```

### Запис з перевіркою

```cpp
#include <fstream>
...
ofstream outFile("output.txt");
if(outFile.is_open()){
    outFile<<"Some text";
    outFile.close();
}else{
    cout<<"Can`t create a file \n";
}
```

## std::filesystem (C++17)
```cpp
#include <filesystem>
...
cout<<filesystem::current_path()<<endl;     //pwd
```