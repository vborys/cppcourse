!!! note "Для нотаток"
    STL (Standart Template Library)

### Контейнери бібліотеки STL

* Послідовні контейнери (array, vector,  forward_list, list, deque)

* Асоціативні контейнери (set, map, multiset, multimap)

* Невпорядковані асоціативні контейнери (unordered_set, unordered_map, unordered_multiset, unordered_multimap)

* Адаптори контейнерів (stack, queue, priority_queue)

* Контейнер span (С++20)









