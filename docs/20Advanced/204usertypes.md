## Користувацькі типи даних

* структури
* об'єднання
* перечислення
* класи


```cpp
typedef unsigned int uint;      //перейменування типів (псевдоніми)

enum Color {red, green, blue};  //перечислення (набір іменованих констант)

struct Car{                     //структура
    int weight;
    int wheel = 4; //значення по замовчуванню (C++11 int wheel {4})
    string name;
};

union Container{   //об'єднання (тип даних, що зберігає одне значення полів одномоментно)
    int int_field;
    double double_field;
};

int main(){
    uint a;
    Color mycolor=Color::green;
    
    Car MyCar;
    MyCar = {950, 4, "Ford"};
    MyCar.name = "Audi";
    
    Car* pMyCar = &MyCar;   //оголошення вказівника на структуру
    pMyCar->name = "Scoda"; //звернення до елементу, або (*pMyCar).name

    Car* MyCar = new Car{}; //структура на "купі"
    MyCar->weight = 1025;
    MyCar->wheel = 4;
    MyCar->name = "Scoda";
    delete MyCar;

}
```

