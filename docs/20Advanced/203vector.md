!!! info "Визначення:"
    std::vector - покращена версія динамічних масивів, представлений ще в C++03

### Оголошення

```cpp
#include <vector>
... 
std::vector<int> array;     //без вказівки довжини
std::vector<int> array2 = { 10, 8, 6, 4, 2, 1 }; 
std::vector<int> array3 { 10, 8, 6, 4, 2, 1 };  //uniform-ініціалізація (з C++11)
```

### Динамічна зміна довжини

```cpp
#include <iostream>
#include <vector>
using namespace std;
int main() {
    vector<int> array { 0, 1, 2 };
    array.resize(5); // зміна довжини
    cout<<"The length is:"<<array.size()<<endl;

for (auto const &element: array)
    cout << element << " "; 
return 0;
}
```


### Додаткові методи опрацювання std::vector

* [Робота з вектором пар (std::vector<std::pair<int, int>>)](https://iq.opengenus.org/vector-of-pairs-in-cpp/)
