### Використання перерахунків
```cpp
enum Students
{
    ANDY,  // 0
    ADIE, // 1
    IHOR,   // 2
    JOHN,   // 3
    LEBRON,  // 4
    MAX_STUDENTS // 5
};
 
int main()
{
    int Students[MAX_STUDENTS]; //всього 5 студентів
    Students[JOHN] = 65;  //звернення до елементу масиву
 
    return 0;
}
```


### Пошук і сортування

```cpp
//бінарний пошук

template <typename T>
bool BinarySearch(const T arr[], int size, T key){
    int leftBound=0, rightBound=size-1;
    do{
        int middle = (leftBound+rightBound)/2
        if (key < arr[middle])
            rightBound=middle-1;
        else if (key > arr[middle])
            leftBound=middle+1;
        else 
            return true;


    }while(leftBound<=rightBound);
    return false;
}

```
