!!! note "Для нотаток"
    Представлений в C++11, std::array — це фіксований масив, який не конвертується у вказівник при передачі в функцію. std::array визначається в заголовковому файлі array, всередині простору імен std. Оголошення змінної std::array наступне

### Оголошення
 
```cpp
#include <array>
 
std::array<int, 4> myarray; // оголошуємо масив типу int довжиною 4
```

### Прямий доступ до значень масиву

```cpp
std::cout << myarray[1];
myarray[2] = 7;
```


### Доступ до елементів перевіркою

```cpp
std::array<int, 4> myarray { 8, 6, 4, 1 };
myarray.at(1) = 7; // елемент масиву під номером 1 - коректний, присвоюємо йому значення 7
myarray.at(8) = 15; // елемент масиву під номером 8 - некоректний, отримуємо помилку
```

### Розмір і сортування

За допомогою функції size() можна дізнатися довжину масиву:
```cpp
#include <iostream>
#include <array>
 
int main()
{
	std::array<double, 4> myarray{ 8.0, 6.4, 4.3, 1.9 };
	std::cout << "length: " << myarray.size();
 
	return 0;
}
```

### Передача масиву в функцію

```cpp
#include <iostream>
#include <array>
 
void printLength(const std::array<double, 4> &myarray)
{
    std::cout << "length: " << myarray.size();
}
 
int main()
{
    std::array<double, 4> myarray { 8.0, 6.4, 4.3, 1.9 };
 
    printLength(myarray);
 
    return 0;
}
```

### Цикл foreach в std::array

```cpp
std::array<int, 4> myarray { 8, 6, 4, 1 };
 
for (auto &element : myarray)
    std::cout << element << ' ';
```


### Сортування std::array

* [Використання std::sort](https://www.digitalocean.com/community/tutorials/sort-in-c-plus-plus)

```cpp
#include <iostream>
#include <array>
#include <algorithm> // для std::sort
 
int main()
{
    std::array<int, 5> myarray { 8, 4, 2, 7, 1 };
    std::sort(myarray.begin(), myarray.end()); // сортування масиву по зростанню
//    std::sort(myarray.rbegin(), myarray.rend()); // сортування масиву по спаданню
 
    for (const auto &element : myarray)
        std::cout << element << ' ';
 
    return 0;
}
```

### Додаткові методи опрацювання масивів

* [Сума елементів std::accumulate](https://stackoverflow.com/questions/3221812/how-to-sum-up-elements-of-a-c-vector)

* [Пошук кількості входжень std::count](https://ru.stackoverflow.com/questions/370381/%D0%95%D1%81%D1%82%D1%8C-%D0%BB%D0%B8-%D0%B2-%D1%81%D1%82%D0%B0%D0%BD%D0%B4%D0%B0%D1%80%D0%BD%D0%BE%D0%B9-%D0%B1%D0%B8%D0%B1%D0%BB%D0%B8%D0%BE%D1%82%D0%B5%D0%BA%D0%B5-%D1%84%D1%83%D0%BD%D0%BA%D1%86%D0%B8%D1%8F-%D0%BF%D0%BE%D0%B4%D1%81%D1%87%D0%B5%D1%82%D0%B0-%D0%BA%D0%BE%D0%BB%D0%B8%D1%87%D0%B5%D1%81%D1%82%D0%B2%D0%B0-%D1%80%D0%B0%D0%B7-%D0%B2%D1%85%D0%BE%D0%B6%D0%B4%D0%B5%D0%BD%D0%B8%D0%B9-%D0%BE%D0%B4%D0%B8%D0%BD%D0%B0%D0%BA)






