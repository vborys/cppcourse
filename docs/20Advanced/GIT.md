## Lesson1. About



### Base config

git config 

git help

### 
git init

git status

git add {file(s)}

git commit -m "Initial commit"

git log
git log -p
git log -n2
git log --grep Some text commit

git diff 

git show e54f       //show commit changes


###
git checkout 21a3   //switch to commit by checksum





## Lesson 2.
git rm somefile     //or manualy
git rm --cached somefile    //rm file from commit

git mv somefile     //or manualy


.gitignore

*.a
Doc/*.txt
Logs/



## Lesson 3. Reset changes

* No add files. 
After modify file(s) you can return previous version (commit)

git restore .

* Added files.

git restore --staged .
git restore .



### Add changes to last commit

1. Edit files
2. git add .
3. git commt --ammend -m "Last commit with changes"

### Switch to any commit(create new commit with old changes)
git revert efc3


### HEAD pointer
git log --online  //show all HEAD hash
git reset --soft HEAD~      //complete reset last commit
git reset --mixed HEAD~
git reset --hard HEAD~


### Delete untrecked files

git clean --force



## Lesson 4. Remote repos on GitHub

git remote add origin https://github.com/...

git push -u origin master


//After push on remote repo on GitHub you can make changes and commit it...


git pull <remote>   //syncro with remote repo

git fetch <remote>  //save syncro with remote repo
git merge 


git clone <url>

## Lesson 5.

### Create and switch to new branch
git branch bugfix
git checkout bugfix

### Merge two branches to master
git checkout master
git merge bugfix
git branch -d bugfix //delete branch


If merge conflict:
1. Fix conflicts manualy
2. git add .
3. git commit

### Rebase two branches (same as merge but nocommiting and fast forward)
git checkout bugfix
git rebase master
git checkout master
git merge bugfix






## Software 

* Sourcetree